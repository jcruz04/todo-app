from flask import Flask, jsonify, abort

app = Flask(__name__)

tasks = [
    {
        'id': 1,
        'title': 'Buy groceries',
        'description': 'Milk, Chicken, Avacado, Fruits',
        'done': False
    },
    {
        'id': 2,
        'title': 'Learn Ansible',
        'description': 'Spin up configured server using Vagrant and Ansible',
        'done': False
    }
]

@app.route('/todo/api/v1/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(400)
    return jsonify({'task': task[0]})

if __name__ == '__main__':
    app.run(debug=True)
